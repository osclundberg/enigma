<?php 
session_start();
$debug = 0;

if($debug){
    header("Content-type:text/xml;charset=utf-8;");
    
}else{
    include('prefix.php');
}


    ?>

<?php  
    include('includes/database.php');

    $returnstring ="";

    $returnstring .= "<? xml version=\"1.0\" encoding=\"UTF-8\"?>";
    $returnstring ="<!DOCTYPE party SYSTEM \"recipe.dtd\">";

    $returnstring .= "<recipes>";

    // Kollar om en sökning är gjord.
    // i så fall, listar alla recept från sökningen sorterat efter mest träffar
    if($_SESSION["tags"]){
        //echo $_SESSION["tags"];
        //die();
        $tags = mysql_real_escape_string($_SESSION["tags"]);

        $tags = explode(":",$tags);

        for ($i=0; $i < count($tags); $i++) { 
            $tags[$i] = str_pad($tags[$i] , strlen($tags[$i]) + 2, "'", STR_PAD_BOTH);
            //echo $tags[$i] . "<br>";
        }

        $tags = implode(" OR Ingredients.name LIKE ", $tags);

        $query = "SELECT Recipes.id, Recipes.name, COUNT(Ingredients.name) AS matchingIngredients, 
                Recipes.cookingtime, Recipes.description, Recipes.instructions,Recipes.portions 
                  FROM IngredientAmount
                  LEFT JOIN Ingredients ON (IngredientAmount.ingredient_id=Ingredients.id)
                  LEFT JOIN Recipes ON IngredientAmount.recipe_id=Recipes.id
                  WHERE Ingredients.name  LIKE $tags 
                  GROUP BY Recipes.name ORDER BY COUNT(Ingredients.name) DESC"
                  ;

        $recipequery = mysql_query($query);     
  
        if(mysql_num_rows($recipequery) == 0){
            echo "<div class='message'>Inga resultat hittade.</div>";

            session_destroy();
            die();
        }
    }
    //Om ej sökning gjord lista alla recept.
    else{
        $recipequery = mysql_query("SELECT * FROM Recipes ORDER BY id DESC");

    }
    unset($_SESSION["tags"]);

?>
    
<?php

    while($line = mysql_fetch_object($recipequery)): 

        $recipeid=$line->id;
        $recipename=$line->name;
        $cookingtime=$line->cookingtime;
        $description=$line->description;
        $instructions=$line->instructions;
        $portions = $line->portions;

        $instructions = explode(";", $instructions);

        $cookingHours = floor($cookingtime / 60);
        $cookingMinutes = $cookingtime % 60;

        
        $returnstring .= "<recipe>";
        $returnstring .= "<id>$recipeid</id>";
        $returnstring .= "<portions>$portions</portions>";
        $returnstring .= "<name>$recipename</name>";
        $returnstring .= "<cookinghours>$cookingHours</cookinghours>";
        $returnstring .= "<cookingminutes>$cookingMinutes</cookingminutes>";
        $returnstring .= "<description>$description</description>";
        $returnstring .= "<instructions><step>";
        $returnstring .= join("</step><step>",$instructions);
        $returnstring .= "</step></instructions>";



        $query = "SELECT IngredientAmount.recipe_id, IngredientAmount.ingredient_id,
                    IngredientAmount.amount_type, IngredientAmount.amount,
                     Ingredients.name,
                    Categories.id, Categories.name AS catname
                   

                FROM IngredientAmount 
                JOIN Ingredients 
                    ON ingredient_id=Ingredients.id 
                JOIN Categories
                    ON Categories.id = Ingredients.category_id
                WHERE recipe_id = $recipeid";


        $result = mysql_query($query)
            or die("Query failed");
            

        $returnstring .= "<ingredients>";
        while ($line = mysql_fetch_object($result)) {



            $ingredient_id = $line->ingredient_id; 
            $amount = $line->amount; 
            $name = $line->name;
            $amount_type = $line->amount_type;
            $catname = $line->catname;

            $querytags = mysql_query("SELECT * FROM Tags WHERE ingredient_id = $ingredient_id");   

            $returnstring = $returnstring . "<ingredient>";
            $returnstring = $returnstring . "<ingredientname>$name</ingredientname>";
            $returnstring = $returnstring . "<category>$catname</category>";
            $returnstring = $returnstring . "<amount type=\"$amount_type\">$amount</amount>";
            if(mysql_num_rows($querytags) != 0){
                $returnstring = $returnstring . "<tags>";
                while($row = mysql_fetch_object($querytags)){ 
                     $returnstring = $returnstring . "<tag>".$row->name. "</tag>";             
                }
                $returnstring = $returnstring . "</tags>";
            }
            $returnstring = $returnstring . "</ingredient>";
        }
        $returnstring .= "</ingredients>";
        $returnstring .= "</recipe>";
    endwhile;
    $returnstring .= "</recipes>";

    print $returnstring; 

    ?>


<?php
if(!$debug){
     $_GET['id'] = "browse";
    include("postfix.php");
}

?>
