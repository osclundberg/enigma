-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Värd: localhost
-- Skapad: 15 dec 2013 kl 15:36
-- Serverversion: 5.5.29
-- PHP-version: 5.4.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databas: `enigma`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `Categories`
--

CREATE TABLE `Categories` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumpning av Data i tabell `Categories`
--

INSERT INTO `Categories` (`id`, `name`) VALUES
(1, 'meat'),
(2, 'vegetable'),
(3, 'carbohydrate'),
(4, 'spice'),
(5, 'other'),
(6, 'dairy');

-- --------------------------------------------------------

--
-- Tabellstruktur `IngredientAmount`
--

CREATE TABLE `IngredientAmount` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `recipe_id` int(4) NOT NULL,
  `ingredient_id` int(4) NOT NULL,
  `amount` float NOT NULL,
  `amount_type` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=281 ;

--
-- Dumpning av Data i tabell `IngredientAmount`
--

INSERT INTO `IngredientAmount` (`id`, `recipe_id`, `ingredient_id`, `amount`, `amount_type`) VALUES
(1, 1, 1, 0.8, 'kg'),
(2, 1, 3, 0.8, 'kg'),
(3, 1, 4, 1, 'st'),
(4, 1, 7, 0.005, 'l'),
(5, 1, 8, 0.005, 'l'),
(6, 1, 9, 1, 'st'),
(7, 1, 10, 0.6, 'kg'),
(8, 1, 1, 100, 'kg'),
(29, 21, 1, 800, 'gram'),
(30, 21, 7, 1.5, 'tsk'),
(31, 21, 0, 1, 'msk'),
(32, 21, 153, 1, 'tsk'),
(33, 21, 146, 1.5, 'msk'),
(34, 21, 154, 15, 'gram'),
(35, 21, 155, 500, 'gram'),
(60, 23, 166, 4, 'st'),
(61, 23, 4, 1, 'st'),
(62, 23, 170, 1, 'st'),
(63, 23, 171, 15, 'gram'),
(64, 23, 172, 1, 'st'),
(126, 26, 156, 1, 'kg'),
(127, 26, 173, 2, 'st'),
(138, 28, 179, 1, 'kg'),
(151, 16, 142, 1, 'kg'),
(152, 16, 143, 1, 'kg'),
(204, 29, 180, 0, 'kg'),
(205, 18, 145, 15, 'gram'),
(206, 18, 139, 3, 'dl'),
(207, 18, 146, 5, 'dl'),
(208, 18, 143, 1, 'kg'),
(209, 18, 147, 3, 'dl'),
(230, 25, 168, 200, 'gram'),
(231, 25, 166, 2, 'st'),
(232, 25, 7, 1, 'krm'),
(233, 25, 8, 1, 'krm'),
(234, 25, 167, 3, 'st'),
(235, 25, 169, 30, 'gram'),
(243, 20, 149, 1, 'st'),
(244, 20, 151, 200, 'gram'),
(245, 20, 7, 1, 'tsk'),
(246, 20, 131, 1, 'krm'),
(247, 20, 152, 1, 'krm'),
(248, 20, 143, 3, 'msk'),
(249, 20, 150, 3, 'msk'),
(250, 24, 157, 1, 'st'),
(251, 24, 158, 2, 'st'),
(252, 24, 159, 1, 'st'),
(253, 24, 160, 1, 'st'),
(254, 24, 162, 50, 'gram'),
(255, 24, 7, 1, 'krm'),
(256, 24, 8, 1, 'krm'),
(257, 24, 165, 1, 'krm'),
(258, 24, 164, 1, 'msk'),
(259, 24, 161, 100, 'gram'),
(260, 24, 163, 2, 'st'),
(271, 27, 1, 600, 'gram'),
(272, 27, 175, 1, 'st'),
(273, 27, 166, 2, 'st'),
(274, 27, 176, 30, 'gram'),
(275, 27, 174, 4, 'st'),
(276, 27, 7, 1, 'krm'),
(277, 27, 8, 1, 'tsk'),
(278, 27, 177, 4, 'msk'),
(279, 27, 164, 4, 'msk'),
(280, 27, 178, 4, 'st');

-- --------------------------------------------------------

--
-- Tabellstruktur `Ingredients`
--

CREATE TABLE `Ingredients` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `category_id` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=181 ;

--
-- Dumpning av Data i tabell `Ingredients`
--

INSERT INTO `Ingredients` (`id`, `name`, `category_id`) VALUES
(1, 'Köttfärs', 1),
(3, 'Tomatsås', 2),
(4, 'gul lök', 2),
(7, 'Salt', 4),
(8, 'Svartpeppar', 4),
(9, 'Köttbuljong', 4),
(10, 'Spaghetti', 3),
(130, 'Curry', 4),
(131, 'Vitpeppar', 4),
(132, 'Äpple', 2),
(134, 'Gädda', 1),
(135, 'Apelsin', 2),
(136, 'Jordnötter', 2),
(137, 'Vattenmelon', 2),
(138, 'Havregryn', 3),
(139, 'Vatten', 5),
(141, 'KÖRV', 1),
(142, 'Banan', 2),
(143, 'Vetemjöl', 6),
(145, 'Saffran', 4),
(146, 'Socker', 5),
(147, 'Mjölk', 6),
(149, 'Broccoli', 1),
(150, 'Grädde', 6),
(151, 'Bacontärningar', 1),
(152, 'Muskotnöt', 4),
(153, 'Rosmarin', 4),
(154, 'Persilja', 2),
(155, 'Basmatiris', 3),
(156, 'Oxfilé', 1),
(157, 'Salladskyckling', 1),
(158, 'Avokado', 2),
(159, 'Cocktailtomater', 2),
(160, 'Gurka', 2),
(161, 'Fetaost', 6),
(162, 'Oliver', 2),
(163, 'creme fraiche', 6),
(164, 'Ketchup', 5),
(165, 'Grillkrydda', 4),
(166, 'tomat', 2),
(167, 'ägg', 6),
(168, 'bacon', 1),
(169, 'parmesan ost', 6),
(170, 'Röd chili', 2),
(171, 'Koriander', 2),
(172, 'Lime', 2),
(173, 'Nudelpaket', 5),
(174, 'Friscobröd', 3),
(175, 'Isbergssallad', 2),
(176, 'Rostad lök', 2),
(177, 'Hamburgardressing', 5),
(178, 'Ostskivor', 6),
(179, 'Jalapeno', 1),
(180, '', 1);

-- --------------------------------------------------------

--
-- Tabellstruktur `Recipes`
--

CREATE TABLE `Recipes` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `cookingtime` int(4) NOT NULL,
  `description` varchar(400) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `instructions` varchar(10000) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `portions` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Dumpning av Data i tabell `Recipes`
--

INSERT INTO `Recipes` (`id`, `name`, `cookingtime`, `description`, `instructions`, `portions`) VALUES
(1, 'Spaghetti med Köttfärsås', 30, 'God rätt som alltid går hem. Den ska dock inte tas för givet.', '1.Stek köttfärsås.;2.Koka Spaghetti.;3.Njut!', 4),
(16, 'Banangröt', 30, 'Den godaste rätten i stan.', 'Mosa banenen;\r\nBlanda i mjölet;\r\nAvnjut din skapelse.', 4),
(18, 'Allans bullar', 150, 'Himmelskt gott, som något du inte ätit förut.', 'Blanda mjöl och vatten i en bunke;Kör i saffranet och sockret.;Varva med mjölk och mer socker.;', 2),
(20, 'Pasta med skinksås', 15, 'Per Morbergs favorit.', 'Koka pastan enligt anvisningar på förpackningen under tiden som du gör skinksåsen.;Skär broccolin i mindre bitar och låt dessa koka med pastan de sista 1-2 minutrarna.;Skär köttet i tärningar om du inte köpt färdigtärnat.;Vispa ut vetemjölet med lite av mjölken (ca 0,75 dl mjölk kan vara lagom till 2,5 msk mjöl). Tillsätt sedan resten av mjölken, lite i taget, under vispning. Tillsätt grädde eller smör. Låt koka upp under omrörning. Sänk plattan och låt koka ett par minuter.;Tillsätt kött-tärningarna och låt koka ytterligare ett par minuter. Smaka av med salt, peppar och eventuellt muskot. Tänk på att olika sorters kött är olika salt så det kan behövas mer eller mindre salt.;Lägg pastan och broccoli på tallrikar och häll skinksåsen över.', 2),
(21, 'Hemmagjord korv', 75, 'En rätt man ej bör ta för givet.', 'Blanda köttfärs, salt, peppar, rosmarin och socker;\nRör om till en härlig smet som du sedan stånkar till en korv;\nGarnera med persilja;\nStek i ugn 1 timme och servera med ris.', 4),
(23, 'Het Salsa', 20, 'Gott till nachos men passar även till chips.', 'Hacka tomat, lök och chili.\r\nHacka koriander och pressa i lime\r\nServera med nachos och gräddfil', 4),
(24, 'Pastasallad à la Syrran', 30, 'En enkel och god sallad. Går utmärkt att ha till stora tillställningar.', 'Börja med att koka pastan, när den är klar kyl den under kallt vatten och ställ den åt sidan.;Medan pastan kokar skär alla grönsaker och gör i ordning såsen.;Blanda alla ihop allting i en stor salladsskål. ', 4),
(25, 'Veronas de Baconas', 480, 'Esta comida es la mejor del mundo!', 'Knäck 3 ägg och lägg de i en skål, vispa tills det är en mjuk, härlig och len sörja.;Ta fram en Corona och öppna den.;Lägg i salt och peppar i äggen och rör om lite till.;Fram med jordnötterna som passar fint med Coronan.;Skär upp tomaterna i 1mm tunna skivor, släng dem och börja om på nytt med att endast tärna dem.;Sätt på We Like To Party (Old School versionen) och ring upp dina 20 "närmaste" polare och 50 randoms som diggar din playlist.; Skiva upp baconen i tärningar, ägna minst 1 timme av kärlek och svett på detta.;Ta fram din 3:e Corona och få o dig 2 jägershots.;Stek bacon och tomaterna tilsammans och lägg den sedan åt sidan och häll i äggen i stekpannan.;Be personerna på bordet att vara försiktiga med mormors lampa och hiva sen i dig 2 öl till, illamåendet är en del av processen.;Stek äggen till en omelett och lägg i tomat/bacon röran i mitten av omeletten för att sedan vika den i två och lägga upp den på en tallrik redo för servering.;Spy upp en av shoten i diskhon och bege dig ut i dimman.;Hamna i bråk, hångla med en cougar för att sedan bli utkastad.;Kryp hemåt och frossa i den finfina fyllemåltiden som nu är redo för dig.', 4),
(26, 'Nudelfest', 10, 'Student de luxe.', 'Koka nudlar;\r\nKoka längre;\r\nÄt', 2),
(27, 'Hamburgare', 40, 'Nu är det fest!', 'Blanda upp köttfärs, salt och peppar i en bunke.;Forma till burgare och stek på svag temperatur tills burgaren är saftig.;Lägg burgaren i bröd med de skivade grönsakerna;I''m lovin it!', 4);

-- --------------------------------------------------------

--
-- Tabellstruktur `Tags`
--

CREATE TABLE `Tags` (
  `tag_id` int(5) NOT NULL AUTO_INCREMENT,
  `ingredient_id` int(4) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumpning av Data i tabell `Tags`
--

INSERT INTO `Tags` (`tag_id`, `ingredient_id`, `name`) VALUES
(1, 1, 'kött'),
(2, 1, 'färs'),
(3, 1, 'nötfärs'),
(4, 1, 'blandfärs'),
(5, 1, 'fläskfärs'),
(6, 4, 'lök'),
(7, 4, 'gul lök'),
(8, 8, 'peppar'),
(9, 9, 'buljong'),
(10, 10, 'pasta'),
(11, 10, 'tagliatelle'),
(12, 156, 'filét mignon');

-- --------------------------------------------------------

--
-- Tabellstruktur `Users`
--

CREATE TABLE `Users` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumpning av Data i tabell `Users`
--

INSERT INTO `Users` (`id`, `username`, `password`) VALUES
(1, 'oscar', '1234'),
(2, 'bjorn', '1234');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
