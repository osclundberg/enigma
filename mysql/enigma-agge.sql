-- phpMyAdmin SQL Dump
-- version 4.0.6
-- http://www.phpmyadmin.net
--
-- Värd: localhost
-- Skapad: 10 dec 2013 kl 11:33
-- Serverversion: 5.5.33
-- PHP-version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databas: `enigma`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `Categories`
--

CREATE TABLE `Categories` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumpning av Data i tabell `Categories`
--

INSERT INTO `Categories` (`id`, `name`) VALUES
(1, 'meat'),
(2, 'vegetable'),
(3, 'carbohydrate'),
(4, 'spice'),
(5, 'other'),
(6, 'dairy');

-- --------------------------------------------------------

--
-- Tabellstruktur `IngredientAmount`
--

CREATE TABLE `IngredientAmount` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `recipe_id` int(4) NOT NULL,
  `ingredient_id` int(4) NOT NULL,
  `amount` float NOT NULL,
  `amount_type` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Dumpning av Data i tabell `IngredientAmount`
--

INSERT INTO `IngredientAmount` (`id`, `recipe_id`, `ingredient_id`, `amount`, `amount_type`) VALUES
(1, 1, 1, 0.8, 'kg'),
(2, 1, 3, 0.8, 'kg'),
(3, 1, 4, 1, 'st'),
(4, 1, 7, 0.005, 'l'),
(5, 1, 8, 0.005, 'l'),
(6, 1, 9, 1, 'st'),
(7, 1, 10, 0.6, 'kg'),
(8, 1, 1, 100, 'kg'),
(12, 16, 142, 1, 'kg'),
(13, 16, 143, 1, 'kg'),
(15, 18, 143, 1, 'kg'),
(16, 18, 145, 15, 'gram'),
(17, 18, 139, 3, 'dl'),
(18, 18, 146, 5, 'dl'),
(19, 18, 147, 3, 'dl'),
(21, 20, 0, 200, 'gram'),
(22, 20, 149, 1, 'st'),
(23, 20, 143, 3, 'msk'),
(24, 20, 150, 3, 'msk'),
(25, 20, 151, 200, 'gram'),
(26, 20, 7, 1, 'tsk'),
(27, 20, 131, 1, 'krm'),
(28, 20, 152, 1, 'krm'),
(29, 21, 1, 800, 'gram'),
(30, 21, 7, 1.5, 'tsk'),
(31, 21, 0, 1, 'msk'),
(32, 21, 153, 1, 'tsk'),
(33, 21, 146, 1.5, 'msk'),
(34, 21, 154, 15, 'gram'),
(35, 21, 155, 500, 'gram'),
(36, 23, 156, 1, 'kg');

-- --------------------------------------------------------

--
-- Tabellstruktur `Ingredients`
--

CREATE TABLE `Ingredients` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `category_id` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=157 ;

--
-- Dumpning av Data i tabell `Ingredients`
--

INSERT INTO `Ingredients` (`id`, `name`, `category_id`) VALUES
(1, 'Köttfärs', 1),
(3, 'Tomatsås', 2),
(4, 'gul lök', 2),
(7, 'Salt', 4),
(8, 'Svartpeppar', 4),
(9, 'Köttbuljong', 4),
(10, 'Spaghetti', 3),
(130, 'Curry', 4),
(131, 'Vitpeppar', 4),
(132, 'Äpple', 2),
(134, 'Gädda', 1),
(135, 'Apelsin', 2),
(136, 'Jordnötter', 2),
(137, 'Vattenmelon', 2),
(138, 'Havregryn', 3),
(139, 'Vatten', 5),
(141, 'KÖRV', 1),
(142, 'Banan', 2),
(143, 'Vetemjöl', 6),
(145, 'Saffran', 4),
(146, 'Socker', 5),
(147, 'Mjölk', 6),
(149, 'Broccoli', 1),
(150, 'Grädde', 6),
(151, 'Bacontärningar', 1),
(152, 'Muskotnöt', 4),
(153, 'Rosmarin', 4),
(154, 'Persilja', 2),
(155, 'Basmatiris', 3),
(156, 'Oxfilé', 1);

-- --------------------------------------------------------

--
-- Tabellstruktur `Recipes`
--

CREATE TABLE `Recipes` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `cookingtime` int(4) NOT NULL,
  `description` varchar(400) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `instructions` varchar(1000) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `portions` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumpning av Data i tabell `Recipes`
--

INSERT INTO `Recipes` (`id`, `name`, `cookingtime`, `description`, `instructions`, `portions`) VALUES
(1, 'Spaghetti med Köttfärsås', 30, 'God rätt som alltid går hem. Den ska dock inte tas för givet.', '1.Stek köttfärsås.;2.Koka Spaghetti.;3.Njut!', 4),
(16, 'Banagröt', 30, 'Den godaste rätten i stan.', '1. Mosa banenen;\r\n2. Blanda i mjölet;\r\n3. Avnjut din skapelse.', 4),
(18, 'Allans bullar', 150, 'Allan har gjort något snuskigt. Du bör inte äta hans bullar...', '1. Blanda mjöl och vatten i en bunke;\n2. Kör i saffranet och sockret.;\n3. Varva med mjölk och mer socker.;', 2),
(20, 'Pasta med skinksås', 15, 'Ganska saftig rätt. Per Morbergs favorit.', 'Koka pastan enligt anvisningar på förpackningen under tiden som du gör skinksåsen.; Skär broccolin i mindre bitar och låt dessa koka med pastan de sista 1-2 minutrarna.;\r\nSkär köttet i tärningar om du inte köpt färdigtärnat.;\r\nVispa ut vetemjölet med lite av mjölken (ca 0,75 dl mjölk kan vara lagom till 2,5 msk mjöl). Tillsätt sedan resten av mjölken, lite i taget, under vispning. Tillsätt grädde eller smör. Låt koka upp under omrörning. Sänk plattan och låt koka ett par minuter.;\r\n\r\nTillsätt kött-tärningarna och låt koka ytterligare ett par minuter. Smaka av med salt, peppar och eventuellt muskot. Tänk på att olika sorters kött är olika salt så det kan behövas mer eller mindre salt.;\r\n\r\nLägg pastan och broccoli på tallrikar och häll skinksåsen över.;', 2),
(21, 'Oscars härliga korv', 75, 'En rätt man ej bör ta för givet.', 'Blanda köttfärs, salt, peppar, rosmarin och socker;\nRör om till en härlig smet som du sedan stånkar till en korv;\nGarnera med persilja;\nStek i ugn 1 timme och servera med ris.', 4),
(22, '', 0, '', '', 0),
(23, 'tomatsås', 60, 'hejhej', 'gott', 2);

-- --------------------------------------------------------

--
-- Tabellstruktur `Tags`
--

CREATE TABLE `Tags` (
  `tag_id` int(5) NOT NULL AUTO_INCREMENT,
  `ingredient_id` int(4) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumpning av Data i tabell `Tags`
--

INSERT INTO `Tags` (`tag_id`, `ingredient_id`, `name`) VALUES
(1, 1, 'kött'),
(2, 1, 'färs'),
(3, 1, 'nötfärs'),
(4, 1, 'blandfärs'),
(5, 1, 'fläskfärs'),
(6, 4, 'lök'),
(7, 4, 'gul lök'),
(8, 8, 'peppar'),
(9, 9, 'buljong'),
(10, 10, 'pasta'),
(11, 10, 'tagliatelle'),
(12, 156, 'filét mignon');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
