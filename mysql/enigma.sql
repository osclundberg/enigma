-- phpMyAdmin SQL Dump
-- version 4.0.6
-- http://www.phpmyadmin.net
--
-- Värd: localhost
-- Skapad: 03 dec 2013 kl 17:51
-- Serverversion: 5.5.33
-- PHP-version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databas: `enigma`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `Categories`
--

CREATE TABLE `Categories` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumpning av Data i tabell `Categories`
--

INSERT INTO `Categories` (`id`, `name`) VALUES
(1, 'meat'),
(2, 'vegetable'),
(3, 'carbohydrate'),
(4, 'spice'),
(5, 'other');

-- --------------------------------------------------------

--
-- Tabellstruktur `IngredientAmount`
--

CREATE TABLE `IngredientAmount` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `recipe_id` int(4) NOT NULL,
  `ingredient_id` int(4) NOT NULL,
  `amount` float NOT NULL,
  `amount_type` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumpning av Data i tabell `IngredientAmount`
--

INSERT INTO `IngredientAmount` (`id`, `recipe_id`, `ingredient_id`, `amount`, `amount_type`) VALUES
(1, 1, 1, 0.8, 'kg'),
(2, 1, 3, 0.8, 'kg'),
(3, 1, 4, 1, 'st'),
(4, 1, 7, 0.005, 'l'),
(5, 1, 8, 0.005, 'l'),
(6, 1, 9, 1, 'st'),
(7, 1, 10, 0.6, 'kg'),
(8, 1, 1, 100, 'kg');

-- --------------------------------------------------------

--
-- Tabellstruktur `Ingredients`
--

CREATE TABLE `Ingredients` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `category_id` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumpning av Data i tabell `Ingredients`
--

INSERT INTO `Ingredients` (`id`, `name`, `category_id`) VALUES
(1, 'Köttfärs', 1),
(3, 'Tomatsås', 2),
(4, 'gul lök', 2),
(7, 'Salt', 4),
(8, 'Svartpeppar', 4),
(9, 'Köttbuljong', 4),
(10, 'Spaghetti', 3);

-- --------------------------------------------------------

--
-- Tabellstruktur `Recipes`
--

CREATE TABLE `Recipes` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `cookingtime` int(4) NOT NULL,
  `description` varchar(400) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `instructions` varchar(1000) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `portions` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumpning av Data i tabell `Recipes`
--

INSERT INTO `Recipes` (`id`, `name`, `cookingtime`, `description`, `instructions`, `portions`) VALUES
(1, 'Spaghetti med Köttfärsås', 30, 'God rätt som alltid går hem. Den ska dock inte tas för givet.', '1.Stek köttfärsås.;2.Koka Spaghetti.;3.Njut!', 4);

-- --------------------------------------------------------

--
-- Tabellstruktur `Tags`
--

CREATE TABLE `Tags` (
  `tag_id` int(5) NOT NULL AUTO_INCREMENT,
  `ingredient_id` int(4) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumpning av Data i tabell `Tags`
--

INSERT INTO `Tags` (`tag_id`, `ingredient_id`, `name`) VALUES
(1, 1, 'kött'),
(2, 1, 'färs'),
(3, 1, 'nötfärs'),
(4, 1, 'blandfärs'),
(5, 1, 'fläskfärs'),
(6, 4, 'lök'),
(7, 4, 'gul lök'),
(8, 8, 'peppar'),
(9, 9, 'buljong'),
(10, 10, 'pasta'),
(11, 10, 'tagliatelle');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
