	jQuery(document).ready(function($) {



	function addOK(el){
			el.removeClass('check-fail');
			el.removeClass('fa-times');
			el.addClass('fa-check');
			el.addClass('check-ok');
	}

	function addFail(el){
		el.removeClass('check-ok');
		el.removeClass('fa-check');
		el.addClass('check-fail');
		el.addClass('fa-times');
	}
		var j = i = 1;
		var units = ["kg","gram", "liter", "dl",  "msk", "tsk", "krm", "st"];

		var categories = {"5":"övrigt", 
		  "1": "kött",  
		  "2": "grönsak", 
		  "3": "kolhydrat",
		  "4": "krydda",
		  "6": "mejeri" 
		};



		$('.recipe-wrapper, .form-wrapper').on('click','#knappen', function(e){
			e.preventDefault();
			j++;
			addRow(j);	

		});


		$('.wrapper').on('click','.erase', function(){
			$(this).parent().slideUp(function(){
				$(this).remove();
			});
			console.log("asdfs");
		});

		$('.wrapper').on('click', '.more-or-less', function(e){
			e.preventDefault();
			showhide($(this));

		});

		function showhide(el){
			console.log('showhide');
			var ingredients = el.parent().find('.ingredients').children();

			if(el.hasClass('hidden')){
				el.removeClass('hidden');
				el.text("visa färre");
				ingredients.show();

			}else{
				el.addClass('hidden');
				el.text("visa fler");
				ingredients.each(function( index ) {

					if(ingredients.length){

					}
					if(index > 2){
						$(this).hide();
					}
				});

			}

		}

		$('.recipes-wrapper').load('get-recipes.php', function(){
			$('.more-or-less').each(function(){
				showhide($(this));
			});
		});

		$('#knappen-mobile').on('click', function(e){
			e.preventDefault();
			j++;
			addRowMobile(i);	

		});

		function addRowMobile(i){
			$('#theList').append("<li id='ingredient_"+ i +"'></li>");
			
			$('#theList li:last').append("<input placeholder='Ingrediens' class='ingredient' type=\"text\" name=\"ingredient_" + i +"\"/>");
			$('#theList li:last').append("<div class='amount-wrapper'></div>");
			$('#theList li:last .amount-wrapper').append("<h4>Mängd:</h4><input class='amount' type=\"text\" name=\"amount_" + i +"\"/>");

			$('#theList li:last .amount-wrapper').append("<select class='amounttype' name=\"amountType_" +i+"\"></select>");	
			$.each(units, function(key, value) {
			     $('#ingredient_' + i + " .amounttype")
			         .append($("<option></option>")
			         .attr("value",value)
			         .text(value)); 
			});
			$('#theList li:last').append("<div class='category-wrapper'></div>");
			$('#theList li:last .category-wrapper').append("<h4>Kategori:</h4><select class='category' name=\"category" +i+"\"></select>");	
			$.each(categories, function(key, value) {   
			     $('#ingredient_' + i + " .category")
			         .append($("<option></option>")
			         .attr("value",key)
			         .text(value)); 
			});
			//$('#theList li:last').append("<span class='check'><i class='fa fa-lg'></i></span>");
		}

		function addRow(i){
				$('#theList').append("<li style='display:none;' id='ingredient_"+ i +"'></li>");
				
				$('#theList li:last').append("<input class='amount' type=\"text\" name=\"amount_" + i +"\"/>");
				$('#theList li:last').append("<select class='amounttype' name=\"amountType_" +i+"\"></select>");	
				$.each(units, function(key, value) {
				     $('#ingredient_' + i + " .amounttype")
				         .append($("<option></option>")
				         .attr("value",value)
				         .text(value)); 
				});
				$('#theList li:last').append("<input class='ingredient' type=\"text\" name=\"ingredient_" + i +"\"/>");
				$('#theList li:last').append("<select class='category' name=\"category" +i+"\"></select>");	
				$.each(categories, function(key, value) {   
				     $('#ingredient_' + i + " .category")
				         .append($("<option></option>")
				         .attr("value",key)
				         .text(value)); 
				});
				$('#theList li:last').append("<span class='check'><i class='fa fa-lg'></i></span>");
				$('#theList li:last').append("<span class='erase'><i class='fa fa-trash-o fa-lg'></i></span>");
				$('#theList li:last').slideDown();
		}

		$('#theList').on('keyup', 'li', function(){
			var	el = $(this);
			if(checkIngredients($(this))){
				addOK(el.find('.check i'));
			}else{
				addFail(el.find('.check i'));
			}

			if(!checkAll()){
				// visa skicka knappen
				//$('input[type=submit]').attr("disabled", "disabled");
			}
			else{
				//dölj skicka knappen
				//$("input[type=submit]").removeAttr("disabled");
			}
		
		});

		function checkAll(){
			//recipeNameOK = checkAlphanumeric(recipeName);
			//cookingTimeOK = checkCookingTime(cookingHours ,cookingMinutes);

			for (var i = 1; i < $('#theList li').size(); i++) {
				if(!checkIngredients($('#ingredient_'+i))){
					return false;
				}
			}
			return true;
		}




		function checkCookingTime(cookingHours, cookingMinutes) {
			var reg = new RegExp(/[^0-9]/);

			if (reg.test(cookingMinutes) || cookingMinutes > 59 ||
				reg.test(cookingHours) ||
				(cookingMinutes.length == 0 && cookingHours.length == 0)) {
				return false;
			}
			return true;
		}

		function checkAlphanumeric(controlledString) {
			var reg = new RegExp(/[^a-z0-9åäöéèàá\s\"-.]/i);
			if (reg.test(controlledString) || controlledString.length == 0) {
				return false;
			}
			return true;
		}

		function checkIngredients(el){
			var amounts = el.find('.amount');
			var ingredient = el.find('.ingredient');

			if(isNaN(amounts.val()) || !amounts.val() ||
			 !checkAlphanumeric(ingredient.val()) || (amounts.val() < 0)){
				return false;
			}   
			return true;
		}



				function getTags(){
					$('.search-field-hidden').val("");
						var str="";
					$.each($('.search-field .search-tag'),function(index, el) {
						if(index+1 != $('.search-field .search-tag').length){
							str += $(el).text()+ ":";

						}else{
							str += $(el).text();
							
						}
						
					});
					$('.search-field-hidden').val(str);
				}

				function addTag(newtag){
					$('.writable-tag').before("<div class='search-tag-wrapper'><div class='search-tag'>"+ newtag +
							"</div><span class='close'>x</span></div>");						
						$('.writable-tag').val("");
				}

				$('#search-form').keydown(function(e){
					if(e.keyCode == 13){
						e.preventDefault();
					}
				});


				var liSelected;
				$('.search-field').keydown(function(e){
					var li = $('.search-field .search-suggestions li');
					if(e.keyCode == 38){
						if(liSelected){
					        liSelected.removeClass('selected');
				            next = liSelected.prev();
				            if(next.length > 0){
				                liSelected = next.addClass('selected');
				            }else{
				                liSelected = li.last().addClass('selected');
        					}	
						}else{
							liSelected = li.last().addClass('selected');

						}

					}
					else if(e.keyCode == 40){
						if(liSelected){
					        liSelected.removeClass('selected');
				            next = liSelected.next();
				            if(next.length > 0){
				                liSelected = next.addClass('selected');
				            }else{
				                liSelected = li.eq(0).addClass('selected');
        					}	
						}else{
							liSelected = li.eq(0).addClass('selected');

						}
					}
					else{
						liSelected = undefined;
					}
				});

				$('.search-field .writable-tag').on('keyup', function(e){
					//console.log($('.search-suggestions').text());
					if(e.keyCode == 38 || e.keyCode == 40) { 
						e.preventDefault();
						return false; 
					}


					$.ajax({
						url: "gethint.php",
						data: {searchvalue : $(this).val()},
						success: function(data){
								if(data == ""){
									$('.search-suggestions').css({'display':'none'});
									console.log(data)
								}else{
									$('.search-suggestions').css({'display':'block'});

								}
								$('.search-suggestions').html(data);

						}
					});
					var inputen =$(this);
					var el = $(this).parent().parent().find('.search-suggestions');

				});
				$('.search-field .search-suggestions').on('mouseover', function(){
					 $('.search-field .search-suggestions li').removeClass('selected');
				});
				$('.search-field').on('click', '.search-suggestions-item',function (){
					if(!$(this).hasClass('search-suggestions-item-recipe')){
						var newtag = $(this).text();
						addTag(newtag);
						getTags();
						$('.search-suggestions').css({'display':'none'});
					}
				});





				$('.search-field').on('click', function(){
					$(' .close').on('click', function(){
						$(this).parent().remove();
						getTags();
					});
				});

				$('.search-field .writable-tag').on('keydown', function(e){
					var hasAnyLiSelected =$('.search-field .search-suggestions li').hasClass('selected');

					if(e.keyCode == 38 || e.keyCode == 40) { 
						e.preventDefault();
					}
					if(e.keyCode == 13 && $('.writable-tag').val() && !hasAnyLiSelected){
						var newtag = $('.writable-tag').val();
						addTag(newtag);
					}
					if(e.keyCode == 13 && hasAnyLiSelected){
						var newtag =$('.search-field .search-suggestions').find('.selected').text();
						addTag(newtag);
						$('.search-field .search-suggestions li').removeClass('selected');

					}
					if(e.keyCode == 8 && !$('.writable-tag').val()){
						$(".search-tag-wrapper:last").remove();
					}
					if(e.keyCode == 13 || e.keyCode == 8){
						getTags();
					}

				});

			});