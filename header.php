<?php session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href='http://fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' type='text/css'>

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,800' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="css/style.css">
		<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
		<script src="javascript/javascript.js"></script>

	</head>


	<body>

		<div class="menubar">

			<ul class="menu">

				<li class="menu-item"><a href="index.php">Hem</a></li>
				<li class="menu-item"><a href="add-recipe-form.php">Lägg till recept</a></li>
				<?php if(!isset($_SESSION['user_id'])){ ?>
					<li class="menu-item"><a href="login.php">Logga in</a></li>
				<?php }else{ ?>
					<li class="menu-item"><a href="logout.php">Logga ut</a></li>
				<?php } ?>
			</ul>
			<div class="clear"></div>
		</div>
		<div class="wrapper">

		<form id="search-form" action="index.php" method="post">
			<input class='search-field-hidden' name="tags" type="text" />
			<div class="search-field">
				<span class=".writable-tag-span"></span><input class="writable-tag"/></span>
				<button><i class="fa fa-search fa-lg"></i></button>
				<div class="search-suggestions"></div>
			</div>
		</form>
