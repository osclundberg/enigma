<?php
	session_start();
	//put XML content in a string
	$xmlstr=ob_get_contents();
	ob_end_clean();
	
	// Load the XML string into a DOMDocument
	$xml = new DOMDocument;
	$xml->loadXML($xmlstr);

	$mode = $_GET['mode'];
	
	// Make a DOMDocument for the XSL stylesheet
	$xsl = new DOMDocument;
	
	// See which user agent is connecting
	$UA = getenv('HTTP_USER_AGENT');
	if (preg_match("/Symbian/", $UA) | preg_match("/Opera/", $UA) | preg_match("/Nokia/", $UA) | preg_match("/iPhone/", $UA)) 
	{
		// if a mobile phone, use a wml stylesheet and set appropriate MIME type
		//header("Content-type:text/vnd.wap.wml");
		//header('Content-type: text/xml; charset=utf-8');

		if($mode == "change") $xsl->load('xsl/recipe-change-mobile.xsl');
		else $xsl->load('xsl/recipe-mobile.xsl');
	} 
	else 
	{
		// if not a mobile phone, use a xml stylesheet
		//header('Content-type: text/xml; charset=utf-8');

		if($mode == "change") $xsl->load('xsl/recipe-change.xsl');
		else $xsl->load('xsl/recipe.xsl');

	}
	
	// Make the transformation and print the result
	$proc = new XSLTProcessor;
	$proc->importStyleSheet($xsl); // attach the xsl rules
	$proc->setParameter('', 'user_logged_in', $_SESSION['user_logged_in']);
	echo $proc->transformToXML($xml);
	?>
