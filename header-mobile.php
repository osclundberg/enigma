<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name = "viewport" content ="width=device-width">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,800' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="css/mobile.css">
		<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
		<script src="javascript/javascript.js"></script>

	</head>


	<body>
		<div class="menubar">

			<ul class="menu">
				<li class="menu-item"><a href="index.php">Hem</a></li>
				<li class="menu-item"><a href="add-recipe-mobile.php">Lägg till recept</a></li>
				<li class="menu-item"><a href="#">Logga in</a></li>
			</ul>
			<div class="clear"></div>
		</div>
		<div class="wrapper">

		<form id="search-form" action="search-handler.php" method="post">
			<input class='search-field-hidden' name="tags" type="text" />
			<div class="search-field">
				<input class="writable-tag"/>
				<button><i class="fa fa-search fa-lg"></i></button>
				<div class="search-suggestions"></div>
			</div>
		</form>