<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:rss="http://purl.org/rss/1.0/"
                xmlns:dc="http://purl.org/dc/elements/1.1/"
                xmlns:syn="http://purl.org/rss/1.0/modules/syndication/"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0">
<xsl:output indent="yes"/>
<xsl:template match="/">

       		<xsl:apply-templates />

</xsl:template>


<xsl:template match="recipes">
	<ul class="recipes">
		<xsl:apply-templates select="recipe"/>
	</ul>
</xsl:template>

<xsl:template match="recipe">
  <li class="recipe">
  	<img src="images/food.png" alt=""/>
  	<div class="description">
  		<h3>
        <a href="recipe-single.php?id={id}"><xsl:value-of select="name"/></a>
        <i class="cookingtime fa fa-clock-o fa-lg"></i>
        <span class=""><xsl:value-of select="cookinghours"/> h</span>
        <span class=""><xsl:value-of select="cookingminutes"/> min</span>
      
  		</h3>
      <p><xsl:value-of select="description"/></p>
      <div class="ingredients-wrapper">
        <span class="ingredients-header">Ingredienser:</span>
        <ul class="ingredients">
          <xsl:apply-templates select="ingredients"/>
        </ul>

        <a class="more-or-less" href="#">Visa färre</a>

      </div>
    </div>
    <div class="clear"></div>
  </li>
</xsl:template>


<xsl:template match="ingredients">
  <xsl:apply-templates select="ingredient"/>
</xsl:template>

<xsl:template match="ingredient">
	<li class="{category}"> <xsl:apply-templates select="ingredientname"/></li>
</xsl:template>

</xsl:stylesheet>

