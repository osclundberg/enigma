<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:rss="http://purl.org/rss/1.0/"
                xmlns:dc="http://purl.org/dc/elements/1.1/"
                xmlns:syn="http://purl.org/rss/1.0/modules/syndication/"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0">
<xsl:output indent="yes" encoding="UTF-8" />
<xsl:template match="/">
      <div class="recipe-wrapper">
          <xsl:apply-templates />
      </div>
</xsl:template>



<xsl:template match="recipe">
      

    <h1><xsl:value-of select="name"/></h1>
    <a href="recipe-single.php?id={id}&amp;mode=change">Ändra</a>
    <img src="images/food.png" alt=""/>
  <div class="content description">
    <h3>Beskrivning</h3>
    <p><xsl:value-of select="description"/></p>
    <i class="cookingtime fa fa-clock-o fa-lg"></i>
    <span class=""><xsl:value-of select="cookingtime"/> min</span>
  </div>
  <div class="content ingredients">
    <h3>Ingredienser</h3>
    <ul>
      <xsl:apply-templates select="ingredients" />
    </ul>
  </div>
  
  <div class="content instructions">
     <!-- <h3>G&ouml;r s&aring; h&auml;r:</h3> -->
    <h3>Instruktioner:</h3> 
    <ol>
      <xsl:apply-templates select="instructions" />
    </ol>
  </div>  
    <!--
    -->
</xsl:template>

<xsl:template match="ingredients">
<xsl:apply-templates select="ingredient"/>
</xsl:template>

<xsl:template match="ingredient">
  <li class="{category}">
    <span class="amount"><xsl:value-of select="amount"/></span>
    <span class="amounttype"><xsl:value-of select="amount/@type" /></span>
    <span class="ingredientname"><xsl:value-of select="ingredientname"/></span>
  </li>
</xsl:template>

<xsl:template match="instructions">
  <xsl:apply-templates select="step" />
</xsl:template>

<xsl:template match="step">
<li><xsl:value-of select="."/></li>
</xsl:template>
<!--
-->
</xsl:stylesheet>

