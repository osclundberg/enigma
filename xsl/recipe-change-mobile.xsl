<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:rss="http://purl.org/rss/1.0/"
                xmlns:dc="http://purl.org/dc/elements/1.1/"
                xmlns:syn="http://purl.org/rss/1.0/modules/syndication/"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0">
<xsl:output indent="yes"/>
<xsl:template match="/">
              <xsl:apply-templates/>
</xsl:template>

<xsl:template match="recipe">
      <div class="form-wrapper">
        <form id="recipeForm" action="add-recipe.php?changeID={id}" accept-charset="utf-8" method="post">
          <input placeholder="Receptnamn" type="text" name="recipeName" value="{name}"/><br/>
            <div class="left">
              <h3><i class="fa fa-comment-o "></i> Beskrivning:</h3>
              <textarea form="recipeForm" name="description"><xsl:value-of select="description"/></textarea>
            </div>

            <div class="right">
              <div class="cookingtime-wrapper">
                <i class="clock fa fa-clock-o fa-lg" style="color: black;"></i>
                Tillagningstid: 
                <input type="number" class='time' placeholder="timmar" name="cookingHours" value="{cookinghours}"/>
                <input type="number" class='time' placeholder="minuter" name="cookingMinutes" value="{cookingminutes}"/>
              </div>
              <div class="portions">
                <i class="clock fa fa-user fa-lg"></i>
                <input type="number" name="portions" min="1" value="{portions}"/>port<br/>

              </div>
          <!--
        -->
            </div>

            <div class="ingredients-wrapper">
              <h3>Ingredienser</h3>
              <ul id="theList">
                <xsl:apply-templates select="ingredients"/>
              </ul>
              <a id="knappen" href="">Lägg till</a>
              <a id="ta-bort" href="">Ta bort</a><br/> 
              
            </div>
            <h3> <i class="fa fa-list-ol"></i>  Instruktioner</h3>
            <textarea name="instructions" id="" cols="30" rows="10"><xsl:value-of select="instructions"/></textarea>
         <input type="submit" id='submit'/>
        </form>
      </div> 
</xsl:template>

<xsl:template match="ingredients">
  <xsl:apply-templates />
</xsl:template>

 <xsl:template match="ingredient">
  
  <li id="ingredient_{1 + count(preceding-sibling::ingredient)}">
    <input class="ingredient" type="text" name="ingredient_{1 + count(preceding-sibling::ingredient)}" value="{ingredientname}"/>
    <div class="amount-wrapper">
      <h4>Mängd:</h4> 



    <input class="amount" type="text" name="amount_{1 + count(preceding-sibling::ingredient)}" value="{amount}"/>

    <select class="amounttype" name="amountType_{1 + count(preceding-sibling::ingredient)}">
    
     <xsl:choose>
      <xsl:when test="amount/@type='kg'">
      <option value="kg" selected="selected">kg</option>
      </xsl:when>
      <xsl:otherwise>
      <option value="kg">kg</option>
      </xsl:otherwise>
   </xsl:choose>

      <xsl:choose>
       <xsl:when test="amount/@type='gram'">
            <option value="gram" selected="selected">gram</option>
       </xsl:when>
      <xsl:otherwise>
            <option value="gram">gram</option>
      </xsl:otherwise>
     </xsl:choose>

      <xsl:choose>
       <xsl:when test="amount/@type='l'">
            <option value="liter" selected="selected">liter</option>
       </xsl:when>
        <xsl:otherwise>
            <option value="liter">liter</option>
        </xsl:otherwise>
       </xsl:choose>

          <xsl:choose>
       <xsl:when test="amount/@type='dl'">
            <option value="dl" selected="selected">dl</option>
        </xsl:when>
        <xsl:otherwise>
            <option value="dl">dl</option>
        </xsl:otherwise>
       </xsl:choose>

        <xsl:choose>
       <xsl:when test="amount/@type='msk'">
            <option value="msk" selected="selected">msk</option>
        </xsl:when>
        <xsl:otherwise>
            <option value="msk">msk</option>
        </xsl:otherwise>
       </xsl:choose>

        <xsl:choose>
       <xsl:when test="amount/@type='tsk'">
            <option value="tsk" selected="selected">tsk</option>
        </xsl:when>
        <xsl:otherwise>
            <option value="tsk">tsk</option>
        </xsl:otherwise>
       </xsl:choose>

       <xsl:choose>
       <xsl:when test="amount/@type='krm'">
            <option value="krm" selected="selected">krm</option>
        </xsl:when>
        <xsl:otherwise>
            <option value="krm">krm</option>
        </xsl:otherwise>
       </xsl:choose>

         <xsl:choose>
       <xsl:when test="amount/@type='st'">
            <option value="st" selected="selected">st</option>
        </xsl:when>
        <xsl:otherwise>
            <option value="st">st</option>
        </xsl:otherwise>
       </xsl:choose>
        
    </select>

    </div>

    <div class="category-wrapper">
      <h4>Kategori:</h4>
           <select class="category" name="category{1 + count(preceding-sibling::ingredient)}">

              <xsl:choose>
               <xsl:when test="category='meat'">
                    <option value="1" selected="selected">kött</option>
                </xsl:when>
                <xsl:otherwise>
                    <option value="1">kött</option>
                </xsl:otherwise>
             </xsl:choose>

             <xsl:choose>
               <xsl:when test="category='vegetable'">
                    <option value="2" selected="selected">grönsak</option>
                </xsl:when>
                <xsl:otherwise>
                    <option value="2">grönsak</option>
                </xsl:otherwise>
             </xsl:choose>

             <xsl:choose>
               <xsl:when test="category='carbohydrate'">
                    <option value="3" selected="selected">kolhydrat</option>
                </xsl:when>
                <xsl:otherwise>
                    <option value="3">kolhydrat</option>
                </xsl:otherwise>
             </xsl:choose>

            <xsl:choose>
               <xsl:when test="category='spice'">
                    <option value="4" selected="selected">krydda</option>
                </xsl:when>
                <xsl:otherwise>
                    <option value="4">krydda</option>
                </xsl:otherwise>
             </xsl:choose> 

               <xsl:choose>
               <xsl:when test="category='other'">
                    <option value="5" selected="selected">övrigt</option>
                </xsl:when>
                <xsl:otherwise>
                    <option value="5">övrigt</option>
                </xsl:otherwise>
             </xsl:choose> 

              <xsl:choose>
               <xsl:when test="category='dairy'">
                    <option value="6" selected="selected">mejeri</option>
                </xsl:when>
                <xsl:otherwise>
                    <option value="6">mejeri</option>
                </xsl:otherwise>
             </xsl:choose>
           </select>
        </div>
        <span class='erase'>
          <i class="fa fa-times-circle fa-lg"></i>
        </span>
  </li>
</xsl:template > 

</xsl:stylesheet>

