<?php

class Database{
	
	private $connection;

	function __construct() {
		$this->open_connection();
	}

	function open_connection(){
		$this->connection = mysql_connect("localhost", "root", "root");
		 mysql_query("set names 'utf8'", $this->connection);
		if (!$this->connection) {
			die("Database connection failed: " . mysql_error());
		} else {
			$db_select = mysql_select_db("enigma", $this->connection);
			if (!$db_select) {
				die("Database selection failed: " . mysql_error());
			}
		}
	}

	public function close_connection() {
		if(isset($this->connection)) {
			mysql_close($this->connection);
			unset($this->connection);
		}
	}

	public function query($sql) {
		$this->last_query = $sql;
		$result = mysql_query($sql, $this->connection);
		$this->confirm_query($result);
		return $result;
	}

	public function hello(){
		echo "hello world";
	}

}

$database = new Database();

?>