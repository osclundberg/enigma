<?php 
	session_start();

	$iphone = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
	$android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");

	if($iphone || $android){
		include('header-mobile.php');
	}else{
		include('header.php');
	}

?>


		<div class="form-wrapper">
			<form id="recipeForm" action="add-recipe.php" accept-charset="utf-8" method="post">
				<h1>Lägg till ett nytt recept</h1>
				<input placeholder="Receptnamn" type="text" name="recipeName" required><br>
				<div class="left">
					<h3>Beskrivning</h3>
					<textarea form="recipeForm" name="description" placeholder="Beskrivning på maträtten"></textarea>
				</div>
				<div class="right">
					<div class="cookingtime-wrapper">
						<i class="fa fa-clock-o fa-lg" style="color: black;"></i>
						<input type="number" class='time' value="0" name="cookingHours" required>h
						<input type="number" class='time' value="0" name="cookingMinutes" min="0" max="59" required>min
					</div>
					<div class="portions-wrapper">
						<i class="fa fa-user fa-lg"></i>
						<input type="number" class='portions' value="4" name="portions" min="1" required>portioner
					</div>

				</div>
				<div class="clear"></div>
				<div class="instructions-wrapper">
					<h3>Instruktioner <span class="explanation">(varje steg separeras med ny rad)</span></h3>
					<textarea name="instructions" placeholder="Gör så här" cols="30" rows="10" required></textarea>
				</div>

				<div class="ingredienser-wrapper">
					<h3>Ingredienser</h3>
						<div class="ingrediens-header">
							<span class="header">Mängd</span>
							<span class="header ">Enhet</span>
							<span class="header ">Ingrediens</span>
							<span class="header ">Kategori</span>
						</div>
					<ul id="theList"></ul>
					<a id="knappen"href="">Lägg till</a>
				</div>


			
				<input type="submit" id='submit'>
			</form>
		</div>
	


<?php include('footer.php');?>
