<?php 

class ingredient {
    public $ingredientName;
    public $amount;
    public $amountType;
    public $category;
    public $ingredientId;

    public function setIngredientName($ingredientName){
    	$this->ingredientName = $ingredientName;
    }

    public function setAmount($amount){
    	$this->amount = $amount;
    }

    public function setAmountType($amountType){
    	if($amountType == "liter"){
    		$amountType = "l";
    	}
    	$this->amountType = $amountType;
    }

    public function setCategory($category){
    	$this->category = $category;
    }

    public function setIngredientId($ingredientId){
    	$this->ingredientId = $ingredientId;
    }

    // public function toString(){
    // 	echo "id: " . $this->ingredientId . " " . $this->amount . " " . $this->amountType  .
    // 		 " " . $this->ingredientName . " " . $this->category;
    // }
}
	$changeID = mysql_real_escape_string($_GET['changeID']);



    $ingredients = array();
    $createNewIngredient = true;
	
	foreach ($_POST as $key => $value){
		 // echo "Field: " . $key . " value: " . $value . "<br>";
		if($createNewIngredient){
			$aIngredient = new ingredient;
			$createNewIngredient = false;
		}
		if(strpos($key, 'amountType') !== false){
			$aIngredient->setAmountType(mysql_real_escape_string($value));
		}
		else if(strpos($key, 'amount') !== false){
			$aIngredient->setAmount(mysql_real_escape_string($value));
		}
		else if(strpos($key, 'ingredient') !== false){
			$aIngredient->setIngredientName(utf8_decode(mysql_real_escape_string($value)));
		}
		else if(strpos($key, 'category') !== false){
			$aIngredient->setCategory(mysql_real_escape_string($value));
			$createNewIngredient = true;
			$ingredients[] = $aIngredient;
		}
	}

	$ingredientIds = array();

	$missing = false;
	$required = array("recipeName", "description", "instructions", "portions");
	
	foreach ($required as $val) {
		if(empty($_POST[$val])){
			$missing = true;
		}
	}
	if($missing) die("missing fields!");

	$recipeName = utf8_decode(mysql_real_escape_string($_POST["recipeName"])); 
	$cookingHours = $_POST["cookingHours"];
	$cookingMinutes = $_POST["cookingMinutes"];
	$description = utf8_decode(mysql_real_escape_string($_POST["description"]));

	$instructionstemp = $_POST['instructions'];
	$steps = preg_split('/\R/', $instructionstemp);
	$steps = array_map('mysql_real_escape_string', $steps);
	$steps = array_map('utf8_decode', $steps);
	$instructions = implode(";", $steps);

	$portions = $_POST["portions"];

	$cookingMinutes = $cookingMinutes + ($cookingHours * 60);

	
	//Connecting to database
	 $link = mysql_connect("localhost", "root", "root") or die("Could not connect");
   	 mysql_select_db("enigma") or die("Could not select database");

	//Create the recipe
   	   if(isset($_GET['changeID'])){
		$recipeQuery = "UPDATE Recipes SET name='$recipeName', cookingtime='$cookingMinutes', description='$description',
					instructions='$instructions', portions='$portions' WHERE id='$changeID'";
		$removeOldIngredientsQuery ="DELETE FROM IngredientAmount
						  WHERE recipe_id='$changeID'";
		mysql_query($removeOldIngredientsQuery);
	 }
	 else{
		$recipeQuery = "INSERT INTO Recipes (name, cookingtime, description, instructions, portions)
					    VALUES ('$recipeName', '$cookingMinutes', '$description', '$instructions', '$portions')";
	 }
	 	
    mysql_query($recipeQuery);


    if($changeID != 0){
    	$recipeID = $changeID;
    	$url = '/enigma/recipe-single.php?id=' . $recipeID . '&message=changed';
    }
    else{
    	$recipeID = mysql_insert_id();
    	$url = '/enigma/recipe-single.php?id=' . $recipeID . '&message=added';
    }

    //Add new ingredients if they not already exists as ingredients or tags.
	for ($i = 0; $i < count($ingredients); $i++) { 	
		$ingredientName = $ingredients[$i]->ingredientName;
		$ingredientCategory = $ingredients[$i]->category;
		$addNewIngredientQuery =  "INSERT INTO Ingredients (name, category_id)
								   SELECT * FROM (SELECT '$ingredientName', '$ingredientCategory' ) AS tmp
								   WHERE
							       NOT EXISTS (SELECT * FROM Tags WHERE name LIKE '$ingredientName')
								   AND NOT EXISTS (SELECT * FROM Ingredients WHERE name LIKE '$ingredientName')";
		mysql_query($addNewIngredientQuery);
		// if(mysql_insert_id() != 0) 
		// 	$ingredients[$i]->setIngredientID(mysql_insert_id());
	}	 
	//Set ids for ingredients;
	for ($i = 0; $i < count($ingredients); $i++) { 	
		$ingredientName = $ingredients[$i]->ingredientName;
		$findIngredientQuery = "SELECT id FROM Ingredients
										WHERE name LIKE '$ingredientName'";
		$result = mysql_query($findIngredientQuery) or die (mysql_error());
		$row = mysql_fetch_row($result);
		$ingredients[$i]->setIngredientId($row[0]);				
	}
	//Finally inserting the ingredients and connects them to the recipe
	for ($i = 0; $i < count($ingredients); $i++){
		$ingredientId = $ingredients[$i]->ingredientId;
		$amount = $ingredients[$i]->amount;
		$amountType = $ingredients[$i]->amountType;
		$query = "INSERT INTO IngredientAmount (recipe_id, ingredient_id, amount, amount_type)
		 VALUES ('$recipeID', '$ingredientId', '$amount', '$amountType')";
		 mysql_query($query) or die(mysql_error());
	}

	
	header("Location: $url");

 ?>