<?php 

	$iphone = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
	$android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");

	if($iphone || $android){
		include('header-mobile.php');
	}else{
		include('header-mobile.php');
	}

?>

<link rel="stylesheet" type="text/css" href="../enigma/css/mobile.css">
		<div class="form-wrapper">
			<form id="recipeForm" action="AddRecepie.php" accept-charset="utf-8" method="post">
				<input placeholder="Receptnamn" type="text" name="recipeName" required><br>
				<div class="left">
					<h3><i class="fa fa-comment-o "></i> Beskrivning</h3>
					<textarea form="recipeForm" name="description" placeholder="Beskrivning på maträtten"></textarea>
				</div>
				<div class="right">
					<div class="cookingtime-wrapper">
						<i class="clock fa fa-clock-o fa-lg"></i>
						<input type="time" class='time' name="cookingTime" required/>
					</div>

					<div class="portions">
						<i class="clock fa fa-user fa-lg"></i>
						<input type="number" value="4" name="portions" min="1" required>port<br>
					</div>

				</div>
				<div class="ingredients-wrapper">
					<h3>Ingredienser</h3>
					<ul id="theList">
						<li>
							<input placeholder="Ingrediens" class="ingredient" type="text" name="ingredient_1">
							<div class="amount-wrapper">
								<h4>Mängd:</h4>
								<input class="amount" type="text" name="amount_1">
								<select class="amounttype" name="amountType_1">
									<option value="kg">kg</option>
									<option value="gram">gram</option>
									<option value="liter">liter</option>
									<option value="dl">dl</option>
									<option value="msk">msk</option>
									<option value="tsk">tsk</option>
									<option value="krm">krm</option>
									<option value="st">st</option>
								</select>
							</div>
							<div class="category-wrapper">
								<h4>Kategori:</h4>
								<select class="category" name="category1">
									<option value="1">kött</option>
									<option value="2">grönsak</option>
									<option value="3">kolhydrat</option>
									<option value="4">krydda</option>
									<option value="5">övrigt</option>
									<option value="6">mejeri</option>
								</select>
							</div>
							<!-- 
							 -->
						</li>
					</ul>
					<a id="knappen-mobile"href="">Lägg till</a><br> 
					<h3> <i class="fa fa-list-ol"></i> Instruktioner</h3>
					<textarea name="instructions" id="" cols="30" rows="10" required></textarea>
					</div>
					<input type="submit" id='submit'>
				</div>
			</form>
		</div>
	
	


<?php include('footer.php');?>
