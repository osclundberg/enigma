<?php 
$debug = 0;

if($debug){
    header("Content-type:text/xml;charset=utf-8;");
    
}else{
    include('prefix.php');
}

 
    $id = 0;
    $mode = 0;
     if ( isset( $_GET['id'] ) && !empty( $_GET['id'] ) ) {
        $id = $_GET['id'];
     }
     if(isset($_GET['mode'])){
        $mode = $_GET['mode'];
     }

    include('includes/database.php');
    
    mysql_query("set names 'utf8'");

    $returnstring ="";
    $returnstring ="<!DOCTYPE party SYSTEM \"recipe.dtd\">";

    // mysql_query("set names 'utf8'");
    // mysql_query("SET CHARACTER SET 'utf8'")<meta charset="utf-8">;
    // $recipequery = mysql_query("SELECT * FROM Recipes");


    $recipequery = mysql_query("SELECT * FROM Recipes WHERE id=$id");
?>
    
<?php
        //ordna
        $line = mysql_fetch_object($recipequery);

        $recipeid=$line->id;
        $recipename=$line->name;
        $cookingtime=$line->cookingtime;
        $description=$line->description;
        $instructions=$line->instructions;
        $portions=$line->portions;

        $instructions = explode(";", $instructions);

        $cookingHours = floor($cookingtime / 60);
        $cookingMinutes = $cookingtime % 60;
        
        $returnstring .= "<recipe>";
        $returnstring .= "<id>$recipeid</id>";
        $returnstring .= "<name>$recipename</name>";
        $returnstring .= "<portions>$portions</portions>";
        $returnstring .= "<cookinghours>$cookingHours</cookinghours>";
        $returnstring .= "<cookingminutes>$cookingMinutes</cookingminutes>";
        $returnstring .= "<description>$description</description>";
        $returnstring .= "<instructions><step>";
        $returnstring .= join("</step><step>",$instructions);
        $returnstring .= "</step></instructions>";

        // en sql-fråga som väljer ut alla rader sorterade fallande på år och vecka
        //    mysql_query("SET NAMES 'utf8'");
        $query = "SELECT IngredientAmount.recipe_id, IngredientAmount.ingredient_id,
                    IngredientAmount.amount_type, IngredientAmount.amount,
                    Ingredients.name,
                    Categories.id, Categories.name AS catname
                   

                FROM IngredientAmount 
                JOIN Ingredients 
                    ON ingredient_id=Ingredients.id 
                JOIN Categories
                    ON Categories.id = Ingredients.category_id
                WHERE recipe_id = $recipeid";

        // utför själva frågan. Om du har fel syntax får du felmeddelandet query failed
        $result = mysql_query($query)
            or die("Query failed");
            
        // loopa över alla resultatrader och skriv ut en motsvarande tabellrad
        $returnstring .= "<ingredients>";
        while ($line = mysql_fetch_object($result)) {
            // lagra innehållet i en tabellrad i variabler
            $ingredient_id = $line->ingredient_id; 
            $amount = $line->amount; 
            $name = $line->name;
            $amount_type = $line->amount_type;
            $catname = $line->catname;
            $querytags = mysql_query("SELECT * FROM Tags WHERE ingredient_id = $ingredient_id");

            $returnstring = $returnstring . "<ingredient>";
            $returnstring = $returnstring . "<ingredientname>$name</ingredientname>";
            $returnstring = $returnstring . "<category>$catname</category>";
            $returnstring = $returnstring . "<amount type=\"$amount_type\">$amount</amount>";
            $returnstring = $returnstring . "<tags>";
            while($row = mysql_fetch_object($querytags)){ 
                    $returnstring = $returnstring . "<tag>". $row->name . "</tag>";              
            }
            $returnstring = $returnstring . "</tags>";
            $returnstring = $returnstring . "</ingredient>";
        }
        $returnstring .= "</ingredients>";
        $returnstring .= "</recipe>";


    print $returnstring; 
    //echo $returnstring;
    ?>

   

<?php
if(!$debug){
    $_GET['mode'] = $mode;
    include('postsinglefix.php');
}

?>
